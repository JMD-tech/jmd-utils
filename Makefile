
CXXFLAGS+= -std=c++17

#TODO: have this by default, but allow overriding
PREFIX=/usr/local

default: all

all: staticlib test

clean:
	rm -f *.o *.a *.so run_test

staticlib: jmd-utils.o
	ar -rcs libjmd-utils.a jmd-utils.o

test: staticlib
	$(CXX) test.cpp libjmd-utils.a -o run_test -std=c++17 -lstdc++fs

install: staticlib
	install -d $(DESTDIR)$(PREFIX)/lib/
	install -m 644 libjmd-utils.a $(DESTDIR)$(PREFIX)/lib/
	install -d $(DESTDIR)$(PREFIX)/include/
	install -m 644 jmd-utils.hpp $(DESTDIR)$(PREFIX)/include/


