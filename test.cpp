#include <iostream>

#include <filesystem>

#include "jmd-utils.hpp"

using std::string; using std::cout; using std::endl;

namespace fs = std::filesystem;

int main( int argc, char** argv )
{
	string tmp = file_get_contents( "test_data/dos_file.txt" );

	tmp = dos2unix( tmp );

	file_put_contents( "test_data/out_unix_file.txt", tmp );

	auto lines = split_string( tmp );

	for ( auto const& l: lines )
		cout << rpad( toupper(trim(l)), 50, "#" ) << endl;

	cout << "\"oui\" is " << (atobool("oui")?"true":"false") << endl;

	cout << "123 pads to " << nnz(123,5) << endl;

	long long ll = 1234567890123456789;

	cout << ll << " pads to " << nnz(ll,25) << endl;

	atomic_write_file( "test_data/out_atomic_written.txt", "SomeData" );

	cout << "millis since epoch: " << getmillis() << endl;

	tmp = "<data1,data2>,function1(childfunc(someparam),otherparam),function2()";
	auto lst = split_list(tmp);
	cout << "split_string(\"" << tmp << "\"):" << endl;
	for (auto const& l: lst)
		cout << l << endl;
	
	cout << "iso_date() = " << iso_date() << ", iso_timestamp() = " << iso_timestamp() << ", hh_mm_ss() = " << hh_mm_ss() << endl;

	tmp = "val1\tval2\nval3   val4\tsuperfluous field\nval5\t\tval6  \t  ignored\none_value_only";

	file_put_contents( "test_data/map_file.tab", tmp );

	auto mymap = load_map_from( "test_data/map_file.tab" );

	for ( auto const& l:mymap )
		cout << l.first << "\t" << l.second << endl;

	cout << "s_to_int(\"0x20\") = " << s_to_int("0x20") << ", s_to_int(\"25\") = " << s_to_int("25") << endl;;

	return 0;
}
