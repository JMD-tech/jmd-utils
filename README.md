## JMD-utils

My own set of generic C++ utility functions.  
See jmd-utils.hpp for details, but a quick summary:  

Mostly simple snippets of codes inspired from various Internet sources (stackoverflow.com):  
- Various string and date/time formatting functions (trim() without Boost libs) and simple conversions
- Port of some PHP functions: file_get/put_contents, explode: split_string
- A getmillis() function

plus some new functions, most notably:  
- Atomic write of string into file (move-replace by temp file)
- A split_string respecting recursive blocks protected (eg "func1(a,b),func2(c,func3(d,e,f)),<data1,data2>" splits into: "func1(a,b)", "func2(c,func3(d,e,f))", "<data1,data2>"), block-enclosings defaults to: (),[],\{\},<>,"",'' but can be overriden to any others. Obviously splitter character too.
- A very tolerant string to bool converter
- And some C functions wrappers for std::string parameters, to avoid uglifying function calls with many .c_str()

