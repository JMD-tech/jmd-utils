#ifndef __JMD_UTILS_HPP__
#define __JMD_UTILS_HPP__

#include <string>
#include <vector>
#include <set>
#include <map>

// ##### String manipulation functions

// Read file into string. No exceptions, return empty string on error
std::string file_get_contents( const std::string& filename );

// Create new or overwrite file, returns false on failure, true on success
bool file_put_contents( const std::string& filename, const std::string& contents );

// Remove all CR to keep LF only
std::string dos2unix( const std::string& in_data );

// Split string by delimiter(s) into vector. Similar to PHP's explode, with this difference:
// With a multiple characters delimiter, it splits by ANY of the characters, not by a string delimiter.
std::vector<std::string> split_string( const std::string& s, const std::string& delim_chars=std::string("\n") );
std::set<std::string> split_string_to_set( const std::string& s, const std::string& delim_chars=std::string("\n") );
// The other way around, equivalent to PHP's implode
//TODO: use template to allow any suitable container
std::string implode( const std::vector<std::string>& container, const std::string& add_separator=std::string("\n") );
std::string implode( const std::set<std::string>& container, const std::string& add_separator=std::string("\n") );

// Like split_string, but doesn't split in string parts enclosed between characters in subblock_opening_chars and their ending counterpart
std::vector<std::string> split_list( const std::string& s, const std::string& delim_chars=",", 
	const std::string& subblock_opening_chars="([{<'\"", const std::string& subblock_closing_chars="" );

// Load TAB-separated values from a file into a map
std::map<std::string,std::string> load_map_from( const std::string& filename, const char* delim_chars=" \t" );
//TODO: allow other types of data

// As of 2021, those are not yet part of std... well well, not really fond of reinventing the wheel, 
//  but rather that than pulling a dependency on Boost on most of my codes for only those 3 trivial functions:
std::string ltrim( const std::string& s, const char* chars=" \t\r\n" );
std::string rtrim( const std::string& s, const char* chars=" \t\r\n" );
std::string trim( const std::string& s, const char* chars=" \t\r\n" );

// As the name says... only treats non accented chars to avoid pulling dependency hell on intl/localization libs 
//   when we only need simple case-insensitive matching on identifiers/options names etc
std::string tolower( const std::string& s );
std::string toupper( const std::string& s );

std::string lpad( const std::string& s, int n, const std::string& c=" " );
std::string rpad( const std::string& s, int n, const std::string& c=" " );

std::string indent( const std::string& in_str, int n );

std::string sql_quote( const std::string& textval );
std::string json_string( const std::string& s );

// ##### Conversion functions

// single shortcut to avoid using .c_str() on parameter
int atoi( const std::string& s );

uint32_t s_to_int( const std::string& s );

std::string hexdump( const void *buf, size_t sz, const std::string& separ );

// Convert string to boolean
bool atobool( const std::string& s );

// Zero-pad integer to string
template <class T> std::string nnz( T num, int n ) { return rpad(std::to_string(num),n,"0"); };

// Filter a string with a character function
bool isalpha( char c );
bool isblank( char c );
bool is_not_blank( char c );
std::string charfilter( const std::string& InStr, bool FilterFunc(char), bool wants=true );

// ##### Date formatting functions
std::string iso_timestamp( struct tm *tp, const std::string& tz );
std::string iso_timestamp( time_t timestamp, const std::string& tz );
std::string iso_timestamp( const std::string& tz="Z" );
std::string iso_date( struct tm *tp );
std::string iso_date( time_t timestamp );
std::string iso_date();
std::string hh_mm_ss( time_t systime, const std::string& sep=":" );
std::string hh_mm_ss( const std::string& sep=":" );
std::string hhmmss();

// ##### Filesystem functions

// Import functions from <cstdio>
int rename( const std::string& oldname, const std::string& newname );
int remove( const std::string& filename );

// Return non-empty path for file (eg . for current/unspecified directory)
std::string parent_path( const std::string& filepath );

// Return name of file without directory
std::string basename( const std::string& filepath );

// Conditionnally add directory separator at end
std::string dirslash( const std::string& dirname );

// ##### Various system functions

// Return milliseconds since epoch
uint64_t getmillis();

// Atomic write string to a file (using move-replace of temp file)
bool atomic_write_file( const std::string& filename, const std::string& contents );

// Execute shell command and return output as string
std::string pipe_command( const std::string& command );

//int udgsock_create( const char* sockname );

#endif
