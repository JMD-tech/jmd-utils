#include "jmd-utils.hpp"

#include <fstream>
#include <algorithm>
#include <set>
#include <random>
#include <chrono>
#include <ctime>
#include <filesystem>

// For pipe_command only
#include <unistd.h>
#include <sys/wait.h>

using std::string; using std::vector; using std::set; using std::map;

/* Many ways to skin a cat: https://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
	...eventually the higher-level methods have higher overhead. But C++ VS C makes no diff at all.
*/
string file_get_contents( const string& filename )
{
	std::ifstream in(filename, std::ios::in | std::ios::binary);
	//TODO: throw exception? or keep it safe?
	if (!in) return "";
	string contents;
	in.seekg(0, std::ios::end);
	contents.resize(in.tellg());
	in.seekg(0, std::ios::beg);
	in.read(&contents[0], contents.size());
	in.close();
	return contents;
}

// Again: https://stackoverflow.com/questions/15388041/how-to-write-stdstring-to-file
//  probably much less a performance  difference in this way. Anyway much less critical too,
//  we won't be spewing hundred MBs into plain txt files, that would be very bad practice.
bool file_put_contents( const string& filename, const string& contents )
{
	bool isok=true;
	try
	{
		std::ofstream out( filename );
		out << contents;
	}
	catch (...)
	{
		isok=false;
	}
	return isok;
}

// Again again: https://stackoverflow.com/questions/20326356/how-to-remove-all-the-occurrences-of-a-char-in-c-string
string dos2unix( const string& in_data )
{
	string resu=in_data;
	//std::erase(resu, '\r'); // In C++20 the future is cleaner :)
	resu.erase(std::remove(resu.begin(), resu.end(), '\r'), resu.end());
	return resu;
}

// Again one more time: https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
vector<string> split_string( const string& s, const string& delim_chars )
{
	vector<string> resu; string token("");
	for ( auto const &c: s )
	{
		if (delim_chars.find_first_of(c)==string::npos)
			token+=c;
		else
		{
			//TODO: Add trim() + condition to ignore empties?
			// or better: options flag
			resu.push_back(token); token="";
		}
	}
	if (token.size()) resu.push_back(token);
	return resu;
}

// Same as above but outputs a set instead of a vector
set<string> split_string_to_set( const string& s, const string& delim_chars )
{
	set<string> resu; string token("");
	for ( auto const &c: s )
	{
		if (delim_chars.find_first_of(c)==string::npos)
			token+=(c);
		else
		{
			//TODO: same as above
			resu.insert(trim(token)); token="";
		}
	}
	if (trim(token).size()) resu.insert(trim(token));
	return resu;
}

string implode( const vector<string>& container, const string& add_separator )
{
	string resu; bool first_el=true;
	for ( auto const &elem: container )
	{
		if (first_el) first_el=false; else resu+=add_separator;
		resu+=elem;
	}

	return resu;
}
// TODO: factorize and expand with templates
string implode( const set<string>& container, const string& add_separator )
{
	string resu; bool first_el=true;
	for ( auto const &elem: container )
	{
		if (first_el) first_el=false; else resu+=add_separator;
		resu+=elem;
	}
	return resu;
}

// Lol, should really make this into a "stackoverflow answers" library: https://stackoverflow.com/questions/216823/how-to-trim-a-stdstring
string ltrim( const string& s, const char* chars )
{
	size_t startpos = s.find_first_not_of(chars);
	return (startpos == string::npos) ? "" : s.substr(startpos);
}

string rtrim( const string& s, const char* chars )
{
	size_t endpos = s.find_last_not_of(chars);
	return (endpos == string::npos) ? "" : s.substr(0, endpos+1);
}

string trim( const string& s, const char* chars )
{
	return rtrim(ltrim(s,chars),chars);
}

string tolower( const string& s )
{
	string resu;
	for ( auto const& c: s ) resu+=tolower(c);
	return resu;
}

string toupper( const string& s )
{
	string resu;
	for ( auto const& c: s ) resu+=toupper(c);
	return resu;
}

string lpad( const string& s, int n, const string& c )
{
	string resu=s;
	while (resu.size()<n) resu+=c;
	return resu.substr(0,n);
}

string rpad( const string& s, int n, const string& c )
{
	string resu=s;
	while (resu.size()<n) resu=c+resu;
	return resu.substr(resu.size()-n,n);
}

string sql_quote( const string& textval )
{
	string resu;
	for ( auto const& c: textval )
		if (c=='\'') resu+="''"; else resu+=c;
	return "'"+resu+"'";
}

int atoi( const std::string& s ) { return atoi(s.c_str()); }

bool atobool( const string& s )
{
	set<string> yval={"t","true","y","yes","ya","d","da","j","ja","si","o","oui","on","e","enable","enabled"};
	string tmp=tolower(trim(s));
	return (atoi(tmp) || (yval.find(tmp)!=yval.end()));
}

//TODO: template for other size and signedness types
uint32_t s_to_int( const string& s )
{
	string tmp=trim(s);
	uint32_t val = 0;
	int base = 10;
	if ( s.size()>2 )
		if ( (s[0]=='0') && (tolower(s[1])=='x') )
		{
			base = 16;
			tmp.erase(tmp.begin()); tmp.erase(tmp.begin());
		}
	//val = std::strtoul();

	std::stringstream ss;
	if (base == 16) ss << std::hex;
	ss << tmp;
	ss >> val;
	return val;
}

string hexdump( const void *buf, size_t sz, const string& separ )
{
	static const char hexcars[]="0123456789ABCDEF";
	string resu; unsigned char *bu=(unsigned char *)buf;
	for(size_t i=0;i<sz;i++)
	{
		if (resu.size()) resu+=separ;
		resu+=hexcars[bu[i]/16]; resu+=hexcars[bu[i]%16];
	}
	return resu;
}


//template <class T> string nnz( T num, int n ) { return rpad(std::to_string(num),n,"0"); }	// template => in .hpp

bool isalpha( char c ) {
	return (toupper(c)>='A' && toupper(c)<='Z') || (string("áàâäéèêëíìîïóòôöúùûüçñÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÇÑ").find(c)!=string::npos);
}

bool isblank( char c ) {
	return (c==' ') || (c=='\t') || (c=='\r') || (c=='\n');
}

bool is_not_blank( char c ) {
	return (c!=' ') && (c!='\t') && (c!='\r') && (c!='\n');
}

string charfilter( const string& InStr, bool FilterFunc(char), bool wants ) {
	string resu;
	for ( auto const& c: InStr ) if (FilterFunc(c)==wants) resu+=c;
	return resu;
}

// Various date formatting functions
// again many different ways... https://stackoverflow.com/questions/9527960/how-do-i-construct-an-iso-8601-datetime-in-c

string iso_timestamp( struct tm *tp, const string& tz )
{
	//TODO: generate UTC if tz = "Z", and make it default? (or find a way to have local tz as default?)
	char buf[30];
	strftime(buf, sizeof(buf), "%FT%T", tp);
	return buf+tz;
}

string iso_timestamp( time_t timestamp, const string& tz )
{
	return iso_timestamp(localtime(&timestamp),tz);
}

string iso_timestamp( const string& tz )
{
	time_t systime; time(&systime);
	return iso_timestamp(systime,tz);
}

string iso_date( struct tm *tp )
{
	char buf[12];
	strftime(buf, sizeof(buf), "%F", tp);
	return buf;
}

string iso_date( time_t timestamp )
{
	return iso_date(localtime(&timestamp));
}

string iso_date()
{
	time_t systime; time(&systime);
	return iso_date(systime);
}

string hh_mm_ss( time_t systime, const std::string& sep )
{
	struct tm *tp;
	tp=localtime(&systime);
	return nnz(tp->tm_hour,2)+sep+nnz(tp->tm_min,2)+sep+nnz(tp->tm_sec,2);
}

string hh_mm_ss( const std::string& sep )
{
	time_t systime; time(&systime);
	return hh_mm_ss(systime,sep);
}

string hhmmss()
{
	return hh_mm_ss("");
}

int rename( const string& oldname, const string& newname ) { return rename(oldname.c_str(),newname.c_str()); }
int remove( const string& filename ) { return remove(filename.c_str()); }

// CAUTION: filesystem's parent_path() can return either "" or "." for the current directory.
// either be sure to have a ".", if we want to add a "/" after, or have it empty for dir-less filename

string parent_path( const string& filepath )
{
	std::filesystem::path fpath(filepath);
	string resu=fpath.parent_path();
	if (!resu.size()) resu=".";
	return resu;
}

string basename( const string& filepath )
{
	std::filesystem::path fpath(filepath);
	return fpath.filename();
}

string dirslash( const string& dirname )
{
	return ((dirname.back()=='/') || (dirname.back()=='\\') || (dirname.back()==std::filesystem::path::preferred_separator))?
		dirname:dirname+std::filesystem::path::preferred_separator;
}

// https://stackoverflow.com/questions/16177295/get-time-since-epoch-in-milliseconds-preferably-using-c11-chrono
uint64_t getmillis()
{
	using namespace std::chrono;
	return duration_cast< milliseconds >( system_clock::now().time_since_epoch() ).count();
}


// Enough with Internet examples and trivialities, now going to my own original functions...

string indent( const string& in_str, int n )
{
	//TODO: allow un-indent with negative n value?
	vector<string> lines = split_string( in_str, "\n" );
	string resu;
	for ( auto const s:lines )
	{
		for (int i = 1; i <= n; i++) resu+='\t';
		resu+=s;
		resu+='\n';		
	}
	return resu;
}

string json_string(const string& s)
{
	string r("\"");
	for ( auto const& c: s )
		switch (c) {
			case '"':
			case '\\':
			case '/':
				r+="\\"+c; break;
			case '\b': r+="\\b"; break;
			case '\f': r+="\\f"; break;
			case '\n': r+="\\n"; break;
			case '\r': r+="\\r"; break;
			case '\t': r+="\\t"; break;
			default:
				r+=c;
		}
	r+="\"";
	return r;
}

vector<string> split_list( const string& s, const string& delim_chars, const string& subblock_opening_chars, const string& subblock_closing_chars  )
{
	vector<string> resu;
	string closing_chars=subblock_closing_chars; const string &opening_chars=subblock_opening_chars;

	// Automatically generate closing chars list from opening list, sensible but overridable defaults
	const string automatch_open="([{<", automatch_close=")]}>";
	for (size_t i=closing_chars.size();i<opening_chars.size();++i)
	{
		char c=opening_chars.at(i);
		auto srch=automatch_open.find_first_of(c);
		if (srch!=string::npos) c=automatch_close.at(srch);
		closing_chars+=c;
	}

	string token, wait_close_chars;
	for ( auto const& c: s )
	{
		// Enter sub-block (add expected closing to wait_close_chars stack) if opening character found
		size_t match_open = opening_chars.find_first_of(c);
		if (match_open!=string::npos) 
			wait_close_chars.push_back(closing_chars.at(match_open));

		size_t match_delim=string::npos;

		if (wait_close_chars.empty())	// Try to split only if we're not in a sub-block
		{
			match_delim=delim_chars.find_first_of(c);
			if (match_delim!=string::npos)
				{ resu.push_back(token); token=""; }
		}
		else	// If we're in a sub-block, look out for its closing character
			if (c==wait_close_chars.back())
				wait_close_chars.pop_back();	// Remove from waited closer chars stack

		// Only case we don't add char is if it's a first level delimiter
		if (match_delim==string::npos) token+=c;
	
	}
	if (token.size()) resu.push_back(token);
	return resu;
}

bool atomic_write_file( const string& filename, const string& contents )
{
	string bdir=dirslash(parent_path(filename));

	// Temp file randomness to prevent race condition
	unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed1);
	std::uniform_int_distribution<int> distribution(0,0xFFFFFF);
	uint32_t rnd = distribution(generator);

	string tfile=bdir+".tmp"+nnz(rnd,8)+basename(filename);	//TODO: use hex formatting instead of decimal
	// Maybe we should not include the filename in case it would be close to the fs max filename/path size limit?

	if (!file_put_contents(tfile,contents))
		return false;

	if (rename(tfile,filename))
	{
		remove(tfile);	// At least try to remove the temporary file... or should we let it here for debugging?
		return false;
	}
	
	return true;
}

map<string,string> load_map_from( const string& filename, const char* delim_chars )
{
	map<string,string> resu; string delims(delim_chars);
	auto lines = split_string( file_get_contents( filename ) );
	for ( auto const& s: lines )
	{
		string val1,val2; bool in_delim=false; size_t part_no=0;
		for ( auto const& c: s )
		{
			if (delims.find_first_of(c)==string::npos)
			{
				in_delim=false;
				(part_no?val2:val1)+=c;
			}
			else
			{
				if (!in_delim)
					++part_no;
				
				in_delim=true;
				if (part_no>=2)
					break;
			}
		}
		resu.emplace(val1,val2);
	}
	return resu;
}

// Execute shell command, return output as string
// TODO: windows version
std::string pipe_command( const std::string& command ) {
	std::string out_buf;
	
	int fd[2]; int old_fd[3];
	pipe(fd);
	
	old_fd[0] = dup(STDIN_FILENO);
	old_fd[1] = dup(STDOUT_FILENO);
	old_fd[2] = dup(STDERR_FILENO);

	int pid = fork();
	if (pid) {
		close(fd[1]);
		dup2(fd[0], STDIN_FILENO);

		size_t bufsz=1024; char buf[bufsz];
		int rc = 1;
		while (rc > 0){
			rc = read(fd[0], buf, bufsz);
			if (rc > 0) out_buf.append(buf, rc);
		}
		if (rc < 0) { // Error
		    out_buf="";
			//TODO: catch signals and errors, handle errno + log
		}
		
		waitpid(pid, NULL, 0);
		close(fd[0]);
		
		//cout << out_buf << endl << "Received data from pipe: " << out_buf.size() << "b" << endl;
	} else {
		close(fd[0]); close(STDOUT_FILENO);
		dup2(fd[1],STDOUT_FILENO);
		
		system(command.c_str());
		
		close(fd[1]);
		exit(0);
	}

	dup2(STDIN_FILENO, old_fd[0]);
	dup2(STDOUT_FILENO, old_fd[1]);
	dup2(STDERR_FILENO, old_fd[2]);
	
	return out_buf;
}

/*
int udgsock_create( const char* sockname )
{
	unlink(sockname); // Security just in case we weren't shut cleanly

	struct sockaddr_un addr;
	addr.sun_family = AF_LOCAL;
	strcpy(addr.sun_path, sockname);
	socklen_t addrlen = SUN_LEN(&addr);
	int sock_fd = socket(PF_LOCAL, SOCK_DGRAM, 0);
	if (sock_fd < 0) return sock_fd;
		//abandon("Server socket creation");
	int b_ret;
	if ( (b_ret=bind(sock_fd, (struct sockaddr *) &addr, addrlen)) < 0) return b_ret;	// TODO: better error return.
		//abandon("Server socket binding");

	chmod(sockname, S_IRWXU | S_IRWXG | S_IRWXO);
	return sock_fd;
}
*/

